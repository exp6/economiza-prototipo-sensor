#include <SoftwareSerial.h>
#include <Wire.h>

#define rxPin 3
#define txPin 2
#define N 60  

const int ledPin = 13;

//Variables para lectura de sensores
float O2;
char O2minStr[50];
float vec_O2[N];
float sum;
int i;
float O2min;

SoftwareSerial mySerial(rxPin, txPin);

// la rutina de setup corre una vez o cuando se presiona reset
void setup() {
  // define pin modes for tx, rx:
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  // inicializa la comunicación serial en 9600 bits por segundo
  Serial.begin(9600);
  mySerial.begin(9600);
  // configuración referencia interna
  analogReference(INTERNAL);

  Serial.println("Esperando conexion a WIFI");
  while(1) {
    if (mySerial.available()>0) {
      char data =mySerial.read();
      if(data == 'K'){
      Serial.println("Escribe cualquier letra:");
      break;
      }
      else
      {
        Serial.write(data);
      }
    }
  }
  Serial.println("Conexion correcta");
}

// la rutina loop corre constantemente
int count = 0;
int j=0;

void loop() {

  // lectura entrada analoga A0:
  int sensorValue = analogRead(A0);
  // Convierte el valor analogo (0 - 1023) a voltaje (0 - 1,1V):
  float voltage = (sensorValue * (1100  / 1024.000))/15;
  
  // if the analog value is high enough, turn on the LED:
//  if (voltage > 200) {
//    Serial.println("ALTO");
//  }   else  {
//    Serial.println("BAJO");
//  }

  O2=-0.1127*voltage+20.974;
  for (i=0;i<=N-2;i++)
  {
    vec_O2[N-1-i]=vec_O2[N-2-i];
  }
  vec_O2[0]=O2;

  // imprime la lectura:
  count = count + 1;

//  Serial.print("ID ");
  Serial.print(String(count));
  
  Serial.print(";");
  Serial.print(voltage);
  Serial.print(";");
  Serial.print(O2);
  Serial.println();

//  dtostrf(O2, 2, 1, O2minStr);
//        Serial.println("dato enviado");
//        mySerial.write(O2minStr, sizeof(O2minStr));

  if (j==N)
  {
    sum=0;
    for (i=1;i<=N;i++)
    {
      sum=sum+vec_O2[i-1];
    }
    O2min=sum/N;

    dtostrf(O2min, 2, 1, O2minStr);
    Serial.println("dato enviado");
    mySerial.write(O2minStr, sizeof(O2minStr));
    j=0;
  }
  j++;
  
  delay(1000);
   
}



