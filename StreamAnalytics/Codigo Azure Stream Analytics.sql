WITH
TelemetryData AS
(
    SELECT
    A.deviceId
    , CAST(A.EventProcessedUtcTime AS datetime) AS event_processed_utc_time
    , CAST(A.EventEnqueuedUtcTime AS datetime) AS event_enqueued_utc_time
    , CAST(A.IoTHub.EnqueuedTime AS datetime) AS event_enqueued_time
    , CAST(A.oxygen AS float) AS oxygen
    , CAST(A.temperature AS float) AS temperature
    , CAST(A.oxygen AS bigint) AS oxygen_ref
    , B.num_kit
    , B.id_estufa_modelo
    FROM
    [InIotHubDevices] A 
    INNER JOIN [InUsuarioEstufaDeviceRef] B
    ON A.deviceId = B.deviceid
),
RegistroMatrizStep1 AS 
(
    SELECT
        A AS BASE,
        A.deviceId,
        CASE WHEN (oxygen - CAST(A.oxygen_ref AS float)) > 0.5 THEN CEILING(A.oxygen)
        WHEN (oxygen - CAST(A.oxygen_ref AS float)) < 0.5 THEN FLOOR(A.oxygen)
        ELSE oxygen END AS oxygen_fixed
    FROM
    TelemetryData A
),
RegistroMatriz AS
(
    SELECT
        A.BASE AS BASE,
        B AS MATRIZ,
        A.deviceId
    FROM
    RegistroMatrizStep1 A
    INNER JOIN [InMatrizDatos] B
    ON A.oxygen_fixed = B.o2
),
CalculoIndicadoresStep1 AS
(
    SELECT
        A.BASE AS BASE,
        A.MATRIZ AS MATRIZ,
        CASE WHEN ((((21.0)-CAST(A.BASE.oxygen AS float))/CAST(A.MATRIZ.co2 AS float)) <= 1.2) 
            AND ((((21.0)-CAST(A.BASE.oxygen AS float))/CAST(A.MATRIZ.co2 AS float)) >= 1.0) 
        THEN '1' 
        ELSE '0' END AS FireStatus, -- 1: BUENA COMBUSTION, 0: MALA COMBUSTION
        0.0 AS OxygenMpFix,
        0.0 AS OxygenCoFix,
        78
        AS N2BalanceFire,
        A.deviceId
    FROM
    RegistroMatriz A 
    --PARTITION BY deviceId
),
CalculoIndicadoresStep2 AS
(
    SELECT
        A.BASE,
        A.MATRIZ,
        A.FireStatus,
        A.OxygenMpFix,
        A.OxygenCoFix,
        CASE WHEN A.N2BalanceFire > 78 
        THEN 78
        ELSE A.N2BalanceFire END AS FinalBalanceValue,
        A.deviceId
    FROM
    CalculoIndicadoresStep1 A 
),
CalculoIndicadores AS
(
    SELECT
        A.BASE,
        A.MATRIZ,
        A.FireStatus,
        A.OxygenMpFix,
        A.OxygenCoFix,
        100 * ((CAST(A.MATRIZ.o2 AS float)-(0.5*(CAST(A.MATRIZ.co_ppm_filter AS float)/10000))) / ((0.264* CAST(A.FinalBalanceValue AS float)) -(CAST(A.MATRIZ.o2 AS float)-(0.5*(CAST(A.MATRIZ.co_ppm_filter AS float)/ 10000)))))
        AS AirExcessPercentage,
        A.deviceId
    FROM
    CalculoIndicadoresStep2 A 
)

-------------- EVENT HUB TO PUBNUB --------------------

SELECT
    A.deviceId,
    A.BASE.num_kit,
    A.BASE.id_estufa_modelo,
    A.MATRIZ.temp AS temperature,
    A.BASE.oxygen AS o2,
    A.MATRIZ.co_ppm_filter,
    A.MATRIZ.co_ppm,
    A.MATRIZ.co_mp_mg_m3_filter,
    A.MATRIZ.co_mp_mg_m3,
    A.FireStatus,
    '0.0' as OxygenMpFix,
    '0.0' as OxygenCoFix,
    A.AirExcessPercentage
INTO
    [outputPubnub]
FROM
    CalculoIndicadores A 

-------------- SQL DATABASE --------------------

SELECT
    A.BASE.num_kit AS [canal_stream],
    A.deviceId AS [device_id],
    A.BASE.oxygen AS [o2],
    A.MATRIZ.temp AS [temp],
    A.MATRIZ.co_ppm_filter AS [co_filtro],
    A.MATRIZ.co_ppm AS [co],
    A.MATRIZ.co_mp_mg_m3_filter AS [mp_filtro],
    A.MATRIZ.co_mp_mg_m3 AS [mp],
    A.FireStatus AS [firestatus],
    0.0 AS [o2_mp_corr],
    0.0 AS [o2_co_corr],
    A.BASE.event_processed_utc_time AS [event_processed_utc_time],
    A.BASE.event_enqueued_utc_time AS [event_enqueued_utc_time],
    A.BASE.event_enqueued_time AS [event_enqueued_time],
    A.BASE.id_estufa_modelo AS [id_estufa_modelo],
    A.MATRIZ.co2 AS [co2]
INTO
    [outputSQL]
FROM
    CalculoIndicadores A 